from bottle import route, run,get,post,request,delete,put
import json

json_data=[{"name":"Rahul","age":25},{"name":"Hiren","age":15},{"name":"Kiran","age":43}]
@get('/getdata')
def getData():
    return json.dumps(json_data)

@post('/postdata')
def postData():
    user=request.json
    # for i in user:
    #     print(i)
    json_data.append(user)
    return json.dumps(json_data)

@get('/get/<name>/')
def getOne(name):
    data=[user for user in json_data if user['name']==name]
    return json.dumps(data)
@delete('/delete/<name>/')
def deleteOne(name):
    data=[user for user in json_data if user['name']==name]
    json_data.remove(data[0])
    return json.dumps(json_data)
@put('/update/<name>/')
def updateData(name):
    data=[user for user in json_data if user['name']==name]
    data[0]['age']=request.json['age']
    return json.dumps(json_data)
    

if __name__ == "__main__":
    run(host='localhost', port=8080, debug=True, reloader=True)